/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Wellington
 */
public class OrderOfService extends Equipment {

    private int os;
    private String date;
    private String status;
    private double valuePiece;
    private double valueHandWork;
    private double valueTotal;
    private String observation;
    private Client client;
/**
 * 
 */
    public OrderOfService() {
    }
/**
 * 
 * @param os
 * @param date
 * @param status
 * @param valuePiece
 * @param valueHandWork
 * @param valueTotal
 * @param observation
 * @param apparatus
 * @param brand
 * @param serialNumber
 * @param model
 * @param defect 
 */
    public OrderOfService(int os, String date, String status, double valuePiece, double valueHandWork, double valueTotal, String observation, String apparatus, String brand, String serialNumber, String model, String defect) {
        super(apparatus, brand, serialNumber, model, defect);
        this.os = os;
        this.date = date;
        this.status = status;
        this.valuePiece = valuePiece;
        this.valueHandWork = valueHandWork;
        this.valueTotal = valueTotal;
        this.observation = observation;
        
    }
    
    
    
    public int getOs() {
        return os;
    }

    public void setOs(int os) {
        this.os = os;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getValuePiece() {
        return valuePiece;
    }

    public void setValuePiece(double valuePiece) {
        this.valuePiece = valuePiece;
    }

    public double getValueHandWork() {
        return valueHandWork;
    }

    public void setValueHandWork(double valueHandWork) {
        this.valueHandWork = valueHandWork;
    }

    public double getValueTotal() {
        return valueTotal;
    }

    public void setValueTotal(double valueTotal) {
        this.valueTotal = valueTotal;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
}

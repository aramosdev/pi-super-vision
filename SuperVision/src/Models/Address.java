/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Wellington
 */
public class Address {

    private String street;
    private String number;
    private String neighborhood;
    private String zipCode;
    private String city;
    private String state;
/**
 * 
 * @param street
 * @param number 
 */
    public Address(String street, String number) {
        this.street = street;
        this.number = number;
    }
/**
 * 
 * @param street
 * @param number
 * @param neighborhood
 * @param zipCode
 * @param city
 * @param state 
 */
    public Address(String street, String number, String neighborhood, String zipCode, String city, String state) {
        this.street = street;
        this.number = number;
        this.neighborhood = neighborhood;
        this.zipCode = zipCode;
        this.city = city;
        this.state = state;
    }

    public Address() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}

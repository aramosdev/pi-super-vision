/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Wellington
 */
public class Equipment  {
    private String apparatus;
    private String brand;
    private String serialNumber;
    private String model;
    private String defect;
    private String piece;
/**
 * 
 */
    public Equipment() {
    }
/**
 * 
 * @param apparatus
 * @param brand
 * @param serialNumber
 * @param model
 * @param defect 
 */
    public Equipment(String apparatus, String brand, String serialNumber, String model, String defect) {
        this.apparatus = apparatus;
        this.brand = brand;
        this.serialNumber = serialNumber;
        this.model = model;
        this.defect = defect;
    }

   public String getApparatus() {
        return apparatus;
    }

    public void setApparatus(String apparatus) {
        this.apparatus = apparatus;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDefect() {
        return defect;
    }

    public void setDefect(String defect) {
        this.defect = defect;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import Utils.ManagerXML;
import Utils.Validation;

/**
 *
 * @author Wellington
 */
public class ManagerOs {

    private ArrayList<OrderOfService> orderOfService;
    private ArrayList<Client> client;
    private int id;
/**
 * 
 */
    public ManagerOs() {
        orderOfService = ManagerXML.readOs();

        if (orderOfService != null && orderOfService.isEmpty()) {
            id = 1;
        } else {
            id = orderOfService.get(orderOfService.size() - 1).getOs() + 1;
        } 
    }
/**
 * 
 * @return 
 */
    public int getId() {
        return id;
    }

    public ArrayList<OrderOfService> getOrderofservice() {
        return orderOfService;
    }

    public void setOrderofservice(ArrayList<OrderOfService> orderOfService) {
        this.orderOfService = orderOfService;
    }
/**
 * 
 * @param os
 * @param cpf
 * @return 
 */
    // Metodo para salvar ordens de serviços 
    public String saveOrderOfService(OrderOfService os, String cpf) {
        ManagerClient m = new ManagerClient();
        client = new ArrayList<>();
        client = m.cpfQuery(cpf);
        if (client == null) {
            return "CPF INVÁLIDO!";
        }
        String[] data = {os.getApparatus(), os.getBrand(), os.getModel(), os.getStatus()};
        if (Validation.validateEmpty(data) == false) {
            return "POR FAVOR, INFORME CAMPOS OBRIGATORIOS.";
        }

        os.setClient(client.get(0));
        orderOfService.add(os);
        ManagerXML.saveOs(orderOfService);
        return "CADASTRO REALIZADO COM SUCESSO!";

    }
/**
 * 
 * @param os
 * @return 
 */
    public ArrayList<OrderOfService> consultOS(int os) {
        ArrayList<OrderOfService> o = new ArrayList<>();
        for (OrderOfService orderOfService1 : orderOfService) {
            if (orderOfService1.getOs() == os) {
                o.add(orderOfService1);
            }
        }
        return o;
    }
/**
 * 
 * @param cpf
 * @return 
 */
    public int queryOsClient(String cpf) {
        for (OrderOfService o : orderOfService) {
            if (o.getClient().getCpf().equalsIgnoreCase(cpf)) {
                return o.getOs();
            }
        }
        return 0;
    }
/**
 * 
 * @param brand
 * @return 
 */
    public ArrayList<OrderOfService> queryBrand(String brand) {
        ArrayList<OrderOfService> o = new ArrayList<>();
        for (OrderOfService orderOfService1 : orderOfService) {
            if (orderOfService1.getBrand().equalsIgnoreCase(brand)) {
                o.add(orderOfService1);
            }
        }
        return o;
    }
/**
 * 
 * @param apparatus
 * @return 
 */
    public ArrayList<OrderOfService> consultApparatus(String apparatus) {
        ArrayList<OrderOfService> o = new ArrayList<>();
        for (OrderOfService orderOfService1 : orderOfService) {
            if (orderOfService1.getApparatus().equalsIgnoreCase(apparatus)) {
                o.add(orderOfService1);
            }
        }
        return o;
    }
/**
 * 
 * @param model
 * @return 
 */
    public ArrayList<OrderOfService> consultModel(String model) {
        ArrayList<OrderOfService> o = new ArrayList<>();
        for (OrderOfService orderOfService1 : orderOfService) {
            if (orderOfService1.getModel().equalsIgnoreCase(model)) {
                o.add(orderOfService1);
            }
        }
        return o;

    }
/**
 * 
 * @param os
 * @return 
 */
    public String removeOs(OrderOfService os) {
        
        orderOfService.remove(os);
        ManagerXML.saveOs(orderOfService);
        return "REMOVIDO COM SUCESSO!";
    }
/**
 * 
 * @param id
 * @param os
 * @return 
 */
    public String changeAllData(int id, OrderOfService os, String cpf) {
        ManagerClient m = new ManagerClient();
        Client c = m.cpfQuery(cpf).get(0);
        for (int i = 0; i < orderOfService.size(); i++) {
            if (id == orderOfService.get(i).getOs()) {
                os.setClient(c);
                orderOfService.set(i, os);
            }
        }

        ManagerXML.saveOs(orderOfService);
        return "REGISTRO ALTERADO COM SUCESSO";
    }
}

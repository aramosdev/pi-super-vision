
package Controller;

import Utils.NameScreens;
import Views.HomeScreenJFrame;
import javax.swing.JFrame;
/**
 * 
 * @author Wellington
 */
public class ControllerMain {
    private HomeScreenJFrame home;
    public ControllerMain() {
        this.home = new HomeScreenJFrame(this);
        this.home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.home.setVisible(true);
    }
    /**
     * 
     * @param screens 
     */
    public void callScreens (NameScreens screens){
        
        switch(screens){
            case RegisterClients:
                ControllerClient controllerClient = new ControllerClient();
                this.home.dispose();
            break;
            case ClientQuery:
                ControllerConsultClient controllerClientQuery = new ControllerConsultClient();
                this.home.dispose();
            break;
            case RegisterServiceOrder:
                ControllerOs controllerOs = new ControllerOs();
                this.home.dispose();
            break;
            case ServiceOrderQuery:
                ControllerConsultOs consultOs = new ControllerConsultOs();
                this.home.dispose();
            break;

            
        }
    }
    
    
    
}

package Controller;

import Utils.NameScreens;
import static Utils.NameScreens.ClientQuery;
import static Utils.NameScreens.RegisterClients;
import static Utils.NameScreens.RegisterServiceOrder;
import static Utils.NameScreens.ServiceOrderQuery;
import Models.Client;
import Models.ManagerClient;
import Views.RegisterClientJFrame;
import java.lang.reflect.Method;
import javax.swing.JFrame;

/**
 * 
 * @author Wellington
 */

public class ControllerClient {

    private RegisterClientJFrame registerClient;
    ManagerClient m;

    public ControllerClient() {

        this.registerClient = new RegisterClientJFrame(this);
        this.registerClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.registerClient.setVisible(true);
        this.m = new ManagerClient();

    }
/***
 * 
 * @param c 
 */
    
    public ControllerClient(Client c) {
        String[] data = {
            c.getName(),
            c.getRg(),
            c.getCpf(),
            c.getStreet(),
            c.getNumber(),
            c.getHomePhone(),
            c.getCellphone(),
            c.getCity(),
            c.getNeighborhood(),
            c.getZipCode(),
            c.getState()
        };

        this.registerClient = new RegisterClientJFrame(this);
        this.registerClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.registerClient.addFields(data);
        this.registerClient.setVisible(true);
        this.m = new ManagerClient();
    }
/**
 * 
 * @param name
 * @param rg
 * @param cpf
 * @param street
 * @param number
 * @param homePhone
 * @param cellphone
 * @param city
 * @param neighborhood
 * @param zipCode
 * @param state
 * @return 
 */
    public String saveClient(String name, String rg, String cpf, String street, String number,
            String homePhone, String cellphone, String city, String neighborhood,
            String zipCode, String state) {

        Client client = new Client(name, rg, cpf, homePhone, cellphone, street, number, neighborhood, zipCode, city, state);
        String result = m.saveClient(client);
        if (result.equalsIgnoreCase("CLIENTE CADASTRADO COM SUCESSO")) {
            ControllerOs os = new ControllerOs();
            this.registerClient.dispose();
        }
        return result;
    }
/**
 * 
 */
    public void returnMain() {
        ControllerMain controllerMain = new ControllerMain();
        this.registerClient.dispose();
    }
    /**
     * 
     * @param screens 
     */

    public void callScreens(NameScreens screens) {
        switch (screens) {
            case RegisterClients:
                ControllerClient controllerClient = new ControllerClient();
                this.registerClient.dispose();
                break;
            case ClientQuery:
                ControllerConsultClient controllerClientQuery = new ControllerConsultClient();
                this.registerClient.dispose();
                break;
            case RegisterServiceOrder:
                ControllerOs controllerOs = new ControllerOs();
                this.registerClient.dispose();
                break;
            case ServiceOrderQuery:
                ControllerConsultOs consultOs = new ControllerConsultOs();
                this.registerClient.dispose();
                break;
  
        }
    }
/**
 * 
 * @param name
 * @param rg
 * @param cpf
 * @param street
 * @param number
 * @param homePhone
 * @param cellphone
 * @param city
 * @param neighborhood
 * @param zipCode
 * @param state
 * @return 
 */
    public String newClients(String name, String rg, String cpf, String street, String number,
            String homePhone, String cellphone, String city, String neighborhood,
            String zipCode, String state) {

        Client client = new Client(name, rg, cpf, homePhone, cellphone, street, number, neighborhood, zipCode, city, state);

        return m.saveClient(client);

    }
/**
 * 
 * @param name
 * @param rg
 * @param cpf
 * @param street
 * @param number
 * @param homePhone
 * @param cellphone
 * @param city
 * @param neighborhood
 * @param zipCode
 * @param state
 * @return 
 */
    public String changeRegister(String name, String rg, String cpf, String street, String number,
            String homePhone, String cellphone, String city, String neighborhood,
            String zipCode, String state) {

        Client client = new Client(name, rg, cpf, homePhone, cellphone, street, number, neighborhood, zipCode, city, state);

        if (m.cpfQuery(cpf) != null) {
            return m.changeAllData(cpf, client);
        }
        try {
            Class c = client.getClass().forName("java.lang.String"); 
            Method m[] = c.getDeclaredMethods();
            for (int i = 0; i < m.length; i++) {
                System.out.println(m[i].toString());
            }
        } catch (Throwable e) {
            System.err.println(e);
        }
        return m.saveClient(client);

    }
}

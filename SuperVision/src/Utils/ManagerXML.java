
package Utils;

import Models.Client;
import Models.ManageUser;
import Models.ManagerClient;
import Models.ManagerOs;
import Models.OrderOfService;
import Models.SystemUser;
import java.util.ArrayList;
import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * 
 * @author Wellington
 */
public class ManagerXML {

    static String relativePath;
    static XStream xStream = new XStream();
    public static final String XML_managerClient = "client_registration";
    public static final String XML_managerOs = "register_os";
    public static final String XML_manageUser = "register_Usuario";

    /**
     * 
     */
    /**
     *@see  Inicializa o xml e as classes
     */
    static public void initialize() {

        xStream.alias(XML_managerClient, ManagerClient.class);//nome do xStream para cliente
        xStream.alias(XML_managerOs, ManagerOs.class);//nome do xStream para os
        xStream.alias(XML_manageUser, ManageUser.class);//nome do xStream para os

        String path = ManagerXML.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        System.out.println(path);//Pegando o caminho relativo
        try {
            relativePath = URLDecoder.decode(path, "UTF-8");
            int lastSlash = relativePath.lastIndexOf("/");
            relativePath = relativePath.substring(0, lastSlash + 1);
            System.out.println("Relative path: " + relativePath);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ManagerXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     * @param Clientes
     */
    static public void saveClients(ArrayList<Client> clients) {
        saveXml(XML_managerClient, clients);
    }
    /**
     * 
     * @param managerOs 
     */
    static public void saveOs(ArrayList<OrderOfService> managerOs){
        saveXml(XML_managerOs, managerOs);
    }
    /**
     * 
     * @param user 
     */
    static public void saveUser(ArrayList<SystemUser> user){
        saveXml(XML_manageUser, user);
    }
    /**
     * 
     * @return Lista de clientes
     */
    static public ArrayList<Client> readClients(){
        return (ArrayList<Client>) xStream.fromXML(fileRead(XML_managerClient));
    }/**
     * 
     * @return Lista de Os 
     */
    static public ArrayList<OrderOfService> readOs(){
        return (ArrayList<OrderOfService>) xStream.fromXML(fileRead(XML_managerOs));
    }
    
    static public ArrayList<ManageUser> readUser(){
        return (ArrayList<ManageUser>) xStream.fromXML(fileRead(XML_manageUser));
    }
    /**
     * 
     * @param fileName
     * @param data 
     */
    static private void saveXml(String fileName, Object data) {
        try {
            FileOutputStream outputStream = new FileOutputStream(relativePath + fileName + ".xml");
            Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
            xStream.toXML(data, writer);
        } catch (Exception e) {
            System.out.println("Erro ao gravar o XML: " + e);
        }

    }
/**
 * 
 * @param name
 * @return 
 */
    static private File fileRead(String name) {
        File file = new File(relativePath + name + ".xml");
        if (!file.isFile()) {
            try {
                FileWriter w = new FileWriter(relativePath + name + ".xml");
                w.write("<list></list>");
                w.close();
                file = new File(relativePath + name + ".xml");
                return file;

            } catch (IOException ex) {
                Logger.getLogger(ManagerXML.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return file;
    }

}

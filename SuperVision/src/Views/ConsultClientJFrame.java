/**
 * 
 */
package Views;

import Controller.ControllerConsultClient;
import Utils.NameScreens;
import Utils.Validation;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * 
 * @author Wellington
 */
public class ConsultClientJFrame extends javax.swing.JFrame {

    ControllerConsultClient controllerConsultClient;
/**
 * 
 * @param c 
 */
    public ConsultClientJFrame(ControllerConsultClient c) {
        controllerConsultClient = c;
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRbGroupSelectField = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableClient = new javax.swing.JTable();
        btnChange = new javax.swing.JButton();
        jrbName = new javax.swing.JRadioButton();
        jbrCpf = new javax.swing.JRadioButton();
        jrbRg = new javax.swing.JRadioButton();
        jrbAll = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        nav = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        navClientQuery = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        navReturnHome = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        navRegisterOs = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        navQuerySystemOrder = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel1.setText("Pesquisar :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(70, 150, 70, 40);
        getContentPane().add(txtSearch);
        txtSearch.setBounds(140, 160, 147, 20);

        btnSearch.setText("Pesquisar");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        getContentPane().add(btnSearch);
        btnSearch.setBounds(300, 140, 100, 42);

        btnRemove.setText("Excluir");
        btnRemove.setPreferredSize(new java.awt.Dimension(104, 29));
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        getContentPane().add(btnRemove);
        btnRemove.setBounds(520, 140, 104, 42);

        btnBack.setText("Voltar");
        btnBack.setPreferredSize(new java.awt.Dimension(104, 29));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        getContentPane().add(btnBack);
        btnBack.setBounds(630, 140, 104, 42);

        jTableClient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nome", "CPF", "RG", "Telefone", "OS", "Endereço"
            }
        ));
        jTableClient.setDebugGraphicsOptions(javax.swing.DebugGraphics.BUFFERED_OPTION);
        jTableClient.setFocusable(false);
        jScrollPane1.setViewportView(jTableClient);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 297, 817, 225);

        btnChange.setText("Alterar");
        btnChange.setPreferredSize(new java.awt.Dimension(104, 29));
        btnChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeActionPerformed(evt);
            }
        });
        getContentPane().add(btnChange);
        btnChange.setBounds(410, 140, 104, 42);

        jrbName.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGroupSelectField.add(jrbName);
        jrbName.setText("Nome");
        jrbName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbNameActionPerformed(evt);
            }
        });
        getContentPane().add(jrbName);
        jrbName.setBounds(260, 70, 60, 23);

        jbrCpf.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGroupSelectField.add(jbrCpf);
        jbrCpf.setText("CPF");
        jbrCpf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbrCpfActionPerformed(evt);
            }
        });
        getContentPane().add(jbrCpf);
        jbrCpf.setBounds(350, 70, 56, 23);

        jrbRg.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGroupSelectField.add(jrbRg);
        jrbRg.setText("RG");
        jrbRg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbRgActionPerformed(evt);
            }
        });
        getContentPane().add(jrbRg);
        jrbRg.setBounds(430, 70, 50, 23);

        jrbAll.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGroupSelectField.add(jrbAll);
        jrbAll.setText("Todos");
        jrbAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbAllActionPerformed(evt);
            }
        });
        getContentPane().add(jrbAll);
        jrbAll.setBounds(500, 70, 70, 23);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/supervision/image/Pagina_inicial3.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 820, 500);

        nav.setText("Cadastro Cliente");
        nav.add(jSeparator2);

        navClientQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        navClientQuery.setText("Cadastrar Cliente");
        navClientQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navClientQueryActionPerformed(evt);
            }
        });
        nav.add(navClientQuery);
        nav.add(jSeparator3);

        navReturnHome.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        navReturnHome.setText("Voltar");
        navReturnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navReturnHomeActionPerformed(evt);
            }
        });
        nav.add(navReturnHome);
        nav.add(jSeparator4);

        jMenuBar1.add(nav);

        jMenu3.setText("Gerar O.S");

        navRegisterOs.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.ALT_MASK));
        navRegisterOs.setText("Gerar O.S");
        navRegisterOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navRegisterOsActionPerformed(evt);
            }
        });
        jMenu3.add(navRegisterOs);
        jMenu3.add(jSeparator5);

        navQuerySystemOrder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK));
        navQuerySystemOrder.setText("Consulta O.S");
        navQuerySystemOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navQuerySystemOrderActionPerformed(evt);
            }
        });
        jMenu3.add(navQuerySystemOrder);
        jMenu3.add(jSeparator6);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(833, 563));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
/**
 * 
 * @param evt 
 */
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        controllerConsultClient.returnMain();
    }//GEN-LAST:event_btnBackActionPerformed
/**
 * 
 * @param evt 
 */
    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        Object[] result = null;
        String search = txtSearch.getText();
        String queryField = Validation.checkedGroup(btnRbGroupSelectField);
        DefaultTableModel modelo = (DefaultTableModel) jTableClient.getModel();
        modelo.setNumRows(0);
        modelo.rowsRemoved(null);
        try {
            result = controllerConsultClient.queryClient(queryField, search);

            if (result.length > 0 || result == null) {
                for (int i = 0; i < result.length; i++) {
                    modelo.addRow((Object[]) result[i]);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Não consta Registro com esse " + queryField);
                modelo.addRow(new Object[]{"Não consta Registro"});
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeActionPerformed
        // TODO add your handling code here:
        try {
            String cpf = jTableClient.getValueAt(jTableClient.getSelectedRow(), 1).toString();
            controllerConsultClient.callChangeClient(cpf);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

    }//GEN-LAST:event_btnChangeActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        // TODO add your handling code here:
        /**
         * 
         */
        String cpf = jTableClient.getValueAt(jTableClient.getSelectedRow(), 1).toString();
        String del;
        Object[] result = null;
        String search = txtSearch.getText();
        String queryField = Validation.checkedGroup(btnRbGroupSelectField);
        DefaultTableModel modelo = (DefaultTableModel) jTableClient.getModel();
        modelo.setNumRows(0);
        modelo.rowsRemoved(null);
        try {
            del = controllerConsultClient.remove(cpf);

            result = controllerConsultClient.queryClient(queryField, search);

            if (result.length > 0 || result == null) {
                for (int i = 0; i < result.length; i++) {
                    modelo.addRow((Object[]) result[i]);
                }
            }
            JOptionPane.showMessageDialog(this, del);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }//GEN-LAST:event_btnRemoveActionPerformed

    private void jrbAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbAllActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(false);
    }//GEN-LAST:event_jrbAllActionPerformed

    private void jrbRgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbRgActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_jrbRgActionPerformed

    private void jbrCpfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbrCpfActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_jbrCpfActionPerformed

    private void jrbNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbNameActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_jrbNameActionPerformed

    private void navClientQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navClientQueryActionPerformed
        // TODO add your handling code here:
        controllerConsultClient.callScreens(NameScreens.RegisterClients);
    }//GEN-LAST:event_navClientQueryActionPerformed

    private void navReturnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navReturnHomeActionPerformed
        // TODO add your handling code here:
        controllerConsultClient.returnMain();
    }//GEN-LAST:event_navReturnHomeActionPerformed

    private void navRegisterOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navRegisterOsActionPerformed
        // TODO add your handling code here:
        controllerConsultClient.callScreens(NameScreens.RegisterServiceOrder);
    }//GEN-LAST:event_navRegisterOsActionPerformed

    private void navQuerySystemOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navQuerySystemOrderActionPerformed
        // TODO add your handling code here:
        controllerConsultClient.callScreens(NameScreens.ServiceOrderQuery);
    }//GEN-LAST:event_navQuerySystemOrderActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnChange;
    private javax.swing.ButtonGroup btnRbGroupSelectField;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTableClient;
    private javax.swing.JRadioButton jbrCpf;
    private javax.swing.JRadioButton jrbAll;
    private javax.swing.JRadioButton jrbName;
    private javax.swing.JRadioButton jrbRg;
    private javax.swing.JMenu nav;
    private javax.swing.JMenuItem navClientQuery;
    private javax.swing.JMenuItem navQuerySystemOrder;
    private javax.swing.JMenuItem navRegisterOs;
    private javax.swing.JMenuItem navReturnHome;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}

package Views;

import Controller.ControllerClient;
import Utils.NameScreens;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class RegisterClientJFrame extends javax.swing.JFrame {

    ControllerClient controllerClient;

    public RegisterClientJFrame(ControllerClient c) {
        controllerClient = c;
        initComponents();
        this.setBackground(Color.WHITE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField9 = new javax.swing.JTextField();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtNameClient = new javax.swing.JTextField();
        txtStreet = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        txtNeighborhood = new javax.swing.JTextField();
        txtRg = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtNumber = new javax.swing.JTextField();
        cboState = new javax.swing.JComboBox();
        txtZipCode = new javax.swing.JTextField();
        txtCpf = new javax.swing.JTextField();
        txtHomePhone = new javax.swing.JTextField();
        txtCellphone = new javax.swing.JTextField();
        btnNew = new javax.swing.JButton();
        btnReturnHome = new javax.swing.JButton();
        btnSave1 = new javax.swing.JButton();
        btnSaveChanges = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        nav = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        navClientQuery = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        navReturnHome = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        navRegisterOs = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        navQuerySystemOrder = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();

        jTextField9.setText("jTextField9");

        jMenu1.setText("jMenu1");

        jMenuItem2.setText("jMenuItem2");

        jMenuItem4.setText("jMenuItem4");

        jMenuItem5.setText("jMenuItem5");

        jMenuItem6.setText("jMenuItem6");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(660, 480));
        setName("RegisterClient"); // NOI18N
        getContentPane().setLayout(null);

        jLabel2.setText("Nome :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(70, 60, 50, 16);

        jLabel3.setText("Rua:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(70, 100, 70, 20);

        jLabel4.setText("Cidade :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(70, 140, 70, 20);

        jLabel5.setText("Bairro :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(70, 180, 70, 20);

        jLabel6.setText("RG :");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(70, 220, 40, 20);

        jLabel7.setText("Telefone Celular : ");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(70, 270, 120, 20);

        jLabel8.setText("nº :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(432, 100, 40, 30);

        jLabel9.setText("CPF :");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(420, 220, 50, 30);

        jLabel10.setText("CEP:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(420, 170, 50, 50);

        jLabel11.setText("UF :");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(424, 136, 50, 40);
        getContentPane().add(txtNameClient);
        txtNameClient.setBounds(190, 60, 390, 28);
        getContentPane().add(txtStreet);
        txtStreet.setBounds(190, 100, 210, 28);
        getContentPane().add(txtCity);
        txtCity.setBounds(190, 140, 190, 28);
        getContentPane().add(txtNeighborhood);
        txtNeighborhood.setBounds(190, 180, 190, 28);
        getContentPane().add(txtRg);
        txtRg.setBounds(190, 220, 190, 28);

        jLabel12.setText("Telefone Residencial :");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(330, 270, 150, 20);
        getContentPane().add(txtNumber);
        txtNumber.setBounds(480, 100, 100, 28);

        cboState.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO" }));
        getContentPane().add(cboState);
        cboState.setBounds(480, 140, 100, 30);
        getContentPane().add(txtZipCode);
        txtZipCode.setBounds(480, 180, 100, 28);
        getContentPane().add(txtCpf);
        txtCpf.setBounds(480, 220, 100, 28);
        getContentPane().add(txtHomePhone);
        txtHomePhone.setBounds(480, 270, 100, 20);
        getContentPane().add(txtCellphone);
        txtCellphone.setBounds(190, 270, 110, 20);

        btnNew.setText("Novo");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        getContentPane().add(btnNew);
        btnNew.setBounds(70, 330, 100, 50);

        btnReturnHome.setText("Voltar");
        btnReturnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnHomeActionPerformed(evt);
            }
        });
        getContentPane().add(btnReturnHome);
        btnReturnHome.setBounds(480, 330, 100, 50);

        btnSave1.setText("Salvar");
        btnSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnSave1);
        btnSave1.setBounds(210, 330, 100, 50);

        btnSaveChanges.setText("Alterar");
        btnSaveChanges.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveChangesActionPerformed(evt);
            }
        });
        getContentPane().add(btnSaveChanges);
        btnSaveChanges.setBounds(350, 330, 100, 50);

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/supervision/image/Pagina_inicial3.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 660, 440);

        nav.setText("Cadastro Cliente");
        nav.add(jSeparator2);

        navClientQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        navClientQuery.setText("Consulta Cliente");
        navClientQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navClientQueryActionPerformed(evt);
            }
        });
        nav.add(navClientQuery);
        nav.add(jSeparator3);

        navReturnHome.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        navReturnHome.setText("Voltar");
        navReturnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navReturnHomeActionPerformed(evt);
            }
        });
        nav.add(navReturnHome);
        nav.add(jSeparator4);

        jMenuBar1.add(nav);

        jMenu3.setText("Gerar O.S");

        navRegisterOs.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.ALT_MASK));
        navRegisterOs.setText("Gerar O.S");
        navRegisterOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navRegisterOsActionPerformed(evt);
            }
        });
        jMenu3.add(navRegisterOs);
        jMenu3.add(jSeparator5);

        navQuerySystemOrder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK));
        navQuerySystemOrder.setText("Consulta O.S");
        navQuerySystemOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navQuerySystemOrderActionPerformed(evt);
            }
        });
        jMenu3.add(navQuerySystemOrder);
        jMenu3.add(jSeparator6);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(660, 482));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:    
        String name = txtNameClient.getText();
        String rg = txtRg.getText();
        String cpf = txtCpf.getText();
        String street = txtStreet.getText();
        String number = txtNumber.getText();
        String homePhone = txtHomePhone.getText();
        String cellphone = txtCellphone.getText();
        String city = txtCity.getText();
        String neighborhood = txtNeighborhood.getText();
        String zipCode = txtZipCode.getText();
        String state = cboState.getSelectedItem().toString();
        
        try {
            String result = controllerClient.newClients(name, rg, cpf, street, number, homePhone, cellphone, city, neighborhood, zipCode, state);
            JOptionPane.showMessageDialog(this, result);
            if(result.equalsIgnoreCase("CLIENTE CADASTRADO COM SUCESSO")){
                clearComp();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Por favor Preencha os campos Corretamente!");
        }
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnReturnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnHomeActionPerformed
        // TODO add your handling code here:
        controllerClient.returnMain();
    }//GEN-LAST:event_btnReturnHomeActionPerformed

    private void navReturnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navReturnHomeActionPerformed
        // TODO add your handling code here:
        controllerClient.returnMain();
    }//GEN-LAST:event_navReturnHomeActionPerformed

    private void navQuerySystemOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navQuerySystemOrderActionPerformed
        // TODO add your handling code here:
        controllerClient.callScreens(NameScreens.ServiceOrderQuery);
    }//GEN-LAST:event_navQuerySystemOrderActionPerformed

    private void navClientQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navClientQueryActionPerformed
        // TODO add your handling code here:
        controllerClient.callScreens(NameScreens.ClientQuery);
    }//GEN-LAST:event_navClientQueryActionPerformed

    private void navRegisterOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navRegisterOsActionPerformed
        // TODO add your handling code here:
        controllerClient.callScreens(NameScreens.RegisterServiceOrder);
    }//GEN-LAST:event_navRegisterOsActionPerformed

    public void clearComp(){
        for (int i = 0; i < getContentPane().getComponentCount(); i++) {
            Component c = getContentPane().getComponent(i);
            if (c instanceof JTextField){
                JTextField field = (JTextField) c;
                field.setText("");
            }
        }
    }
    
    private void btnSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave1ActionPerformed
        // TODO add your handling code here:
        String name = txtNameClient.getText();
        String rg = txtRg.getText();
        String cpf = txtCpf.getText();
        String street = txtStreet.getText();
        String number = txtNumber.getText();
        String homePhone = txtHomePhone.getText();
        String cellphone = txtCellphone.getText();
        String city = txtCity.getText();
        String neighborhood = txtNeighborhood.getText();
        String zipCode = txtZipCode.getText();
        String state = cboState.getSelectedItem().toString();
                
        try {
            String result = controllerClient.saveClient(name, rg, cpf, street, number, homePhone, cellphone, city, neighborhood, zipCode, state);
            JOptionPane.showMessageDialog(this, result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Por favor Preencha os campos Corretamente!");
        }
        
        
    }//GEN-LAST:event_btnSave1ActionPerformed

    private void btnSaveChangesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveChangesActionPerformed
        // TODO add your handling code here:
        String name = txtNameClient.getText();
        String rg = txtRg.getText();
        String cpf = txtCpf.getText();
        String street = txtStreet.getText();
        String number = txtNumber.getText();
        String homePhone = txtHomePhone.getText();
        String cellphone = txtCellphone.getText();
        String city = txtCity.getText();
        String neighborhood = txtNeighborhood.getText();
        String zipCode = txtZipCode.getText();
        String state = cboState.getSelectedItem().toString();
                
        try {
            String result = controllerClient.changeRegister(name, rg, cpf, street, number, homePhone, cellphone, city, neighborhood, zipCode, state);
            JOptionPane.showMessageDialog(this, result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Por favor Preencha os campos Corretamente!");
        }
    }//GEN-LAST:event_btnSaveChangesActionPerformed

    public void addFields(String[] data){
       txtNameClient.setText(data[0]);
       txtRg.setText(data[1]);
       txtCpf.setText(data[2]);
       txtStreet.setText(data[3]);
       txtNumber.setText(data[4]);
       txtHomePhone.setText(data[5]);
       txtCellphone.setText(data[6]);
       txtCity.setText(data[7]);
       txtNeighborhood.setText(data[8]);
       txtZipCode.setText(data[9]);
       cboState.setToolTipText(data[10]);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnReturnHome;
    private javax.swing.JButton btnSave1;
    private javax.swing.JButton btnSaveChanges;
    private javax.swing.JComboBox cboState;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JMenu nav;
    private javax.swing.JMenuItem navClientQuery;
    private javax.swing.JMenuItem navQuerySystemOrder;
    private javax.swing.JMenuItem navRegisterOs;
    private javax.swing.JMenuItem navReturnHome;
    private javax.swing.JTextField txtCellphone;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtCpf;
    private javax.swing.JTextField txtHomePhone;
    private javax.swing.JTextField txtNameClient;
    private javax.swing.JTextField txtNeighborhood;
    private javax.swing.JTextField txtNumber;
    private javax.swing.JTextField txtRg;
    private javax.swing.JTextField txtStreet;
    private javax.swing.JTextField txtZipCode;
    // End of variables declaration//GEN-END:variables
}

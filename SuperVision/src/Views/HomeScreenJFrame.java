
package Views;


import Controller.ControllerMain;
import Utils.NameScreens;
import java.awt.Color;

public class HomeScreenJFrame extends javax.swing.JFrame {
     
     private ControllerMain controllerMain;
     public static final Color COLOR_OUVER = new Color(0,0,153);

    public HomeScreenJFrame(ControllerMain c) {
        controllerMain = c;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem4 = new javax.swing.JMenuItem();
        lbRegisterClient = new javax.swing.JLabel();
        lbClientQuery = new javax.swing.JLabel();
        lbRegisterOrderSystem = new javax.swing.JLabel();
        lbOrderSystemQuery = new javax.swing.JLabel();
        lbExit = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        navOs = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        navQuerySystemOrder = new javax.swing.JMenuItem();

        jMenuItem4.setText("jMenuItem4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(640, 474));
        setType(java.awt.Window.Type.UTILITY);
        getContentPane().setLayout(null);

        lbRegisterClient.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        lbRegisterClient.setForeground(new java.awt.Color(0, 0, 153));
        lbRegisterClient.setText("Cadastro de cliente");
        lbRegisterClient.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRegisterClientMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbRegisterClientMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbRegisterClientMouseEntered(evt);
            }
        });
        getContentPane().add(lbRegisterClient);
        lbRegisterClient.setBounds(30, 60, 180, 60);

        lbClientQuery.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        lbClientQuery.setForeground(new java.awt.Color(0, 0, 153));
        lbClientQuery.setText("Consulta de cliente");
        lbClientQuery.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbClientQueryMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbClientQueryMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbClientQueryMouseEntered(evt);
            }
        });
        getContentPane().add(lbClientQuery);
        lbClientQuery.setBounds(30, 90, 180, 60);

        lbRegisterOrderSystem.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        lbRegisterOrderSystem.setForeground(new java.awt.Color(0, 0, 153));
        lbRegisterOrderSystem.setText("Ordem de serviço");
        lbRegisterOrderSystem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRegisterOrderSystemMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbRegisterOrderSystemMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbRegisterOrderSystemMouseEntered(evt);
            }
        });
        getContentPane().add(lbRegisterOrderSystem);
        lbRegisterOrderSystem.setBounds(30, 120, 180, 60);

        lbOrderSystemQuery.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        lbOrderSystemQuery.setForeground(new java.awt.Color(0, 0, 153));
        lbOrderSystemQuery.setText("Consultar ordem de serviço");
        lbOrderSystemQuery.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbOrderSystemQueryMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbOrderSystemQueryMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbOrderSystemQueryMouseEntered(evt);
            }
        });
        getContentPane().add(lbOrderSystemQuery);
        lbOrderSystemQuery.setBounds(30, 150, 220, 60);

        lbExit.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        lbExit.setForeground(new java.awt.Color(0, 0, 153));
        lbExit.setText("Sair");
        lbExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbExitMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbExitMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbExitMouseEntered(evt);
            }
        });
        getContentPane().add(lbExit);
        lbExit.setBounds(30, 230, 180, 50);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/supervision/image/Pagina_inicial1.png"))); // NOI18N
        jLabel8.setMaximumSize(new java.awt.Dimension(700, 455));
        jLabel8.setMinimumSize(new java.awt.Dimension(700, 455));
        getContentPane().add(jLabel8);
        jLabel8.setBounds(0, 0, 640, 430);

        jMenu2.setText("Cadastro Cliente");

        jMenuItem2.setText("Cadastrar Cliente");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);
        jMenu2.add(jSeparator1);

        jMenuItem5.setText("Consulta Cliente");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);
        jMenu2.add(jSeparator5);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Criar O.S.");

        navOs.setText("Gerar O.s");
        navOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navOsActionPerformed(evt);
            }
        });
        jMenu3.add(navOs);
        jMenu3.add(jSeparator2);

        navQuerySystemOrder.setText("Consultar O.s");
        navQuerySystemOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navQuerySystemOrderActionPerformed(evt);
            }
        });
        jMenu3.add(navQuerySystemOrder);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(655, 490));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbRegisterClientMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRegisterClientMouseEntered
        // TODO add your handling code here:
        mouseOver(lbRegisterClient,null );
    }//GEN-LAST:event_lbRegisterClientMouseEntered

    private void lbRegisterClientMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRegisterClientMouseExited
        // TODO add your handling code here:
        mouseOver(null, lbRegisterClient);
    }//GEN-LAST:event_lbRegisterClientMouseExited

    private void lbClientQueryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbClientQueryMouseEntered
        mouseOver(lbClientQuery, null);
    }//GEN-LAST:event_lbClientQueryMouseEntered

    private void lbClientQueryMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbClientQueryMouseExited
        mouseOver(null, lbClientQuery);
    }//GEN-LAST:event_lbClientQueryMouseExited

    private void lbRegisterOrderSystemMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRegisterOrderSystemMouseEntered
        // TODO add your handling code here:
        mouseOver(lbRegisterOrderSystem, null);
    }//GEN-LAST:event_lbRegisterOrderSystemMouseEntered

    private void lbRegisterOrderSystemMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRegisterOrderSystemMouseExited
        // TODO add your handling code here:
        mouseOver(null, lbRegisterOrderSystem);
    }//GEN-LAST:event_lbRegisterOrderSystemMouseExited

    private void lbOrderSystemQueryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbOrderSystemQueryMouseEntered
        // TODO add your handling code here:
        mouseOver(lbOrderSystemQuery, null);
    }//GEN-LAST:event_lbOrderSystemQueryMouseEntered

    private void lbOrderSystemQueryMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbOrderSystemQueryMouseExited
        // TODO add your handling code here:
        mouseOver(null, lbOrderSystemQuery);
    }//GEN-LAST:event_lbOrderSystemQueryMouseExited

    private void lbExitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbExitMouseEntered
        // TODO add your handling code here:
        mouseOver(lbExit, null);
    }//GEN-LAST:event_lbExitMouseEntered

    private void lbExitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbExitMouseExited
        // TODO add your handling code here:
        mouseOver(null, lbExit);
    }//GEN-LAST:event_lbExitMouseExited

    private void lbRegisterClientMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRegisterClientMouseClicked
            controllerMain.callScreens(NameScreens.RegisterClients);
    }//GEN-LAST:event_lbRegisterClientMouseClicked

    private void lbExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbExitMouseClicked
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_lbExitMouseClicked

    private void lbClientQueryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbClientQueryMouseClicked
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.ClientQuery);
    }//GEN-LAST:event_lbClientQueryMouseClicked

    private void lbRegisterOrderSystemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRegisterOrderSystemMouseClicked
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.RegisterServiceOrder);
    }//GEN-LAST:event_lbRegisterOrderSystemMouseClicked

    private void lbOrderSystemQueryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbOrderSystemQueryMouseClicked
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.ServiceOrderQuery);
    }//GEN-LAST:event_lbOrderSystemQueryMouseClicked

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.RegisterClients);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.ClientQuery);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void navOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navOsActionPerformed
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.RegisterServiceOrder);
    }//GEN-LAST:event_navOsActionPerformed

    private void navQuerySystemOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navQuerySystemOrderActionPerformed
        // TODO add your handling code here:
        controllerMain.callScreens(NameScreens.ServiceOrderQuery);
    }//GEN-LAST:event_navQuerySystemOrderActionPerformed
   
    public void mouseOver(javax.swing.JLabel labelEntered, javax.swing.JLabel labelExit){
        if(labelEntered == null){
            labelExit.setForeground(COLOR_OUVER);
        }else{
            labelEntered.setForeground(Color.RED);
        }
   }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    public javax.swing.JLabel lbClientQuery;
    public javax.swing.JLabel lbExit;
    public javax.swing.JLabel lbOrderSystemQuery;
    public javax.swing.JLabel lbRegisterClient;
    public javax.swing.JLabel lbRegisterOrderSystem;
    private javax.swing.JMenuItem navOs;
    private javax.swing.JMenuItem navQuerySystemOrder;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerOs;
import Utils.NameScreens;
import Utils.Validation;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class RegisterOrderService extends javax.swing.JFrame {

    ControllerOs controllerOs;

    public RegisterOrderService(ControllerOs c) {
        controllerOs = c;
        initComponents();
        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
        Date dataAtual = new Date(System.currentTimeMillis());
        txtDate.setText(sd.format(dataAtual));
        String i = Integer.toString(controllerOs.getId());
        txtIdOs.setText(i);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButton2 = new javax.swing.JRadioButton();
        btnRbtGrounp = new javax.swing.ButtonGroup();
        btnSave = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        txtObservation = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtValueTotal = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtValueHandWork = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtPiece = new javax.swing.JTextField();
        txtValuePiece = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtBrand = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtModel = new javax.swing.JTextField();
        txtEquipment = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtIdOs = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCpf = new javax.swing.JTextField();
        txtDate = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        rbtApproved = new javax.swing.JRadioButton();
        rbtAwaiting = new javax.swing.JRadioButton();
        rbtNotApproved = new javax.swing.JRadioButton();
        rbtAwaitingBudget = new javax.swing.JRadioButton();
        btnReturn = new javax.swing.JButton();
        txtSerialNumber = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        navQuerySystemOrder = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        navReturnHome = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        nav = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        navRegisterClient = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        navClientQuery = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();

        jRadioButton2.setText("jRadioButton2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        btnSave.setText("Salvar");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        getContentPane().add(btnSave);
        btnSave.setBounds(286, 489, 100, 50);

        btnNew.setText("Novo");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        getContentPane().add(btnNew);
        btnNew.setBounds(100, 489, 100, 50);
        getContentPane().add(txtObservation);
        txtObservation.setBounds(204, 392, 382, 60);

        jLabel15.setText("Observação :");
        getContentPane().add(jLabel15);
        jLabel15.setBounds(100, 392, 86, 29);

        jLabel9.setText("Status da OS : ");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(100, 301, 90, 20);

        txtValueTotal.setEnabled(false);
        getContentPane().add(txtValueTotal);
        txtValueTotal.setBounds(511, 250, 110, 28);

        jLabel13.setText("Total :");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(450, 240, 60, 40);

        txtValueHandWork.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtValueHandWorkMouseExited(evt);
            }
        });
        getContentPane().add(txtValueHandWork);
        txtValueHandWork.setBounds(190, 260, 130, 28);

        jLabel6.setText("Mão de Obra :");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(100, 254, 80, 30);

        jLabel5.setText("Peças :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(100, 215, 70, 30);

        txtPiece.setText(" ");
        getContentPane().add(txtPiece);
        txtPiece.setBounds(188, 215, 132, 28);

        txtValuePiece.setToolTipText("");
        txtValuePiece.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtValuePieceMouseExited(evt);
            }
        });
        getContentPane().add(txtValuePiece);
        txtValuePiece.setBounds(466, 216, 160, 28);

        jLabel12.setText("Valor das Peças :");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(348, 215, 112, 30);
        getContentPane().add(txtDescription);
        txtDescription.setBounds(260, 180, 367, 28);

        jLabel4.setText("Defeito constatado :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(100, 175, 120, 30);

        jLabel3.setText("Marca :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(100, 130, 60, 20);
        getContentPane().add(txtBrand);
        txtBrand.setBounds(182, 130, 190, 28);

        jLabel14.setText("Modelo :");
        getContentPane().add(jLabel14);
        jLabel14.setBounds(423, 87, 68, 30);
        getContentPane().add(txtModel);
        txtModel.setBounds(509, 81, 116, 28);
        getContentPane().add(txtEquipment);
        txtEquipment.setBounds(186, 95, 190, 28);

        jLabel2.setText("Aparelho :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(100, 90, 80, 30);

        jLabel1.setText("O.S :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(100, 41, 40, 30);

        txtIdOs.setEnabled(false);
        getContentPane().add(txtIdOs);
        txtIdOs.setBounds(140, 40, 82, 30);

        jLabel7.setText("Cpf cliente :");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(230, 40, 70, 30);
        getContentPane().add(txtCpf);
        txtCpf.setBounds(302, 41, 106, 30);

        txtDate.setText(" ");
        txtDate.setEnabled(false);
        getContentPane().add(txtDate);
        txtDate.setBounds(509, 42, 116, 28);

        jLabel10.setText("Data :");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(418, 31, 50, 50);

        rbtApproved.setBackground(new java.awt.Color(255, 255, 255));
        btnRbtGrounp.add(rbtApproved);
        rbtApproved.setText("Aprovado");
        getContentPane().add(rbtApproved);
        rbtApproved.setBounds(179, 300, 90, 23);

        rbtAwaiting.setBackground(new java.awt.Color(255, 255, 255));
        btnRbtGrounp.add(rbtAwaiting);
        rbtAwaiting.setText("Aguardando");
        getContentPane().add(rbtAwaiting);
        rbtAwaiting.setBounds(179, 342, 110, 23);

        rbtNotApproved.setBackground(new java.awt.Color(255, 255, 255));
        btnRbtGrounp.add(rbtNotApproved);
        rbtNotApproved.setText("Não Aprovada");
        getContentPane().add(rbtNotApproved);
        rbtNotApproved.setBounds(352, 302, 110, 23);

        rbtAwaitingBudget.setBackground(new java.awt.Color(255, 255, 255));
        btnRbtGrounp.add(rbtAwaitingBudget);
        rbtAwaitingBudget.setText("Aguardando Orçamento");
        getContentPane().add(rbtAwaitingBudget);
        rbtAwaitingBudget.setBounds(352, 343, 170, 23);

        btnReturn.setText("Voltar");
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });
        getContentPane().add(btnReturn);
        btnReturn.setBounds(486, 489, 100, 50);
        getContentPane().add(txtSerialNumber);
        txtSerialNumber.setBounds(509, 130, 116, 28);

        jLabel8.setText("Séria :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(419, 120, 72, 40);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/supervision/image/Pagina_inicial3.png"))); // NOI18N
        getContentPane().add(jLabel11);
        jLabel11.setBounds(0, 0, 710, 580);

        jMenu3.setText("Gerar O.S");

        navQuerySystemOrder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK));
        navQuerySystemOrder.setText("Consulta O.S");
        navQuerySystemOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navQuerySystemOrderActionPerformed(evt);
            }
        });
        jMenu3.add(navQuerySystemOrder);
        jMenu3.add(jSeparator5);

        navReturnHome.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        navReturnHome.setText("Voltar");
        navReturnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navReturnHomeActionPerformed(evt);
            }
        });
        jMenu3.add(navReturnHome);
        jMenu3.add(jSeparator6);

        jMenuBar1.add(jMenu3);

        nav.setText("Cadastro Cliente");
        nav.add(jSeparator2);

        navRegisterClient.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        navRegisterClient.setText("Cadastro Cliente");
        navRegisterClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navRegisterClientActionPerformed(evt);
            }
        });
        nav.add(navRegisterClient);
        nav.add(jSeparator1);

        navClientQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        navClientQuery.setText("Consulta Cliente");
        navClientQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navClientQueryActionPerformed(evt);
            }
        });
        nav.add(navClientQuery);
        nav.add(jSeparator4);

        jMenuBar1.add(nav);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(723, 642));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        try {
            controllerOs.returnMain();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_btnReturnActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        String cpf = txtCpf.getText();
        String brand = txtBrand.getText();
        String date = txtDate.getText();
        String defect = txtDescription.getText();
        String apparatus = txtEquipment.getText();
        int idOs = Validation.convertValueInt(txtIdOs.getText());
        String model = txtModel.getText();
        String observation = txtObservation.getText();
        String piece = txtPiece.getText();
        String serialNumber = txtSerialNumber.getText();
        double valueHankWork = Validation.convertValueDouble(txtValueHandWork.getText());
        double valuePiece = Validation.convertValueDouble(txtValuePiece.getText());
        double valueTotal = Validation.convertValueDouble(txtValueTotal.getText());
        String status = Validation.checkedGroup(btnRbtGrounp);

        try {
            String result = controllerOs.saveOs(idOs, date, status, valuePiece, valueHankWork, valueTotal, observation, cpf, apparatus, brand, serialNumber, model, defect, piece);
            JOptionPane.showMessageDialog(this, result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        String cpf = txtCpf.getText();
        String brand = txtBrand.getText();
        String date = txtDate.getText();
        String defect = txtDescription.getText();
        String apparatus = txtEquipment.getText();
        int idOs = Validation.convertValueInt(txtIdOs.getText());
        String model = txtModel.getText();
        String observation = txtObservation.getText();
        String piece = txtPiece.getText();
        String serialNumber = txtSerialNumber.getText();
        double valueHankWork = Validation.convertValueDouble(txtValueHandWork.getText());
        double valuePiece = Validation.convertValueDouble(txtValuePiece.getText());
        double valueTotal = Validation.convertValueDouble(txtValueTotal.getText());
        String status = Validation.checkedGroup(btnRbtGrounp);

        try {
            String result = controllerOs.newRegister(idOs, date, status, valuePiece, valueHankWork, valueTotal, observation, cpf, apparatus, brand, serialNumber, model, defect, piece);
            JOptionPane.showConfirmDialog(rootPane, result);
            if (result.equals("CADASTRO REALIZADO COM SUCESSO!")) {
                clearComp();
                String i = Integer.toString(controllerOs.getId());
                txtIdOs.setText(i);
            }
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e.getMessage());
        }
    }//GEN-LAST:event_btnNewActionPerformed

    private void navClientQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navClientQueryActionPerformed
        // TODO add your handling code here:
        controllerOs.callScreens(NameScreens.ClientQuery);
    }//GEN-LAST:event_navClientQueryActionPerformed

    private void navReturnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navReturnHomeActionPerformed
        // TODO add your handling code here:
        controllerOs.returnMain();
    }//GEN-LAST:event_navReturnHomeActionPerformed

    private void navQuerySystemOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navQuerySystemOrderActionPerformed
        // TODO add your handling code here:
        controllerOs.callScreens(NameScreens.ServiceOrderQuery);
    }//GEN-LAST:event_navQuerySystemOrderActionPerformed

    private void navRegisterClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navRegisterClientActionPerformed
        // TODO add your handling code here:
        controllerOs.callScreens(NameScreens.RegisterClients);
    }//GEN-LAST:event_navRegisterClientActionPerformed

    private void txtValuePieceMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtValuePieceMouseExited
        // TODO add your handling code here:
        double value1 = Validation.convertValueDouble(txtValuePiece.getText());
        double value2 = Validation.convertValueDouble(txtValueHandWork.getText());
        double valueTotal = value1 + value2;
        txtValueTotal.setText(Double.toString(valueTotal));
    }//GEN-LAST:event_txtValuePieceMouseExited

    private void txtValueHandWorkMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtValueHandWorkMouseExited
        // TODO add your handling code here:
        double value1 = Validation.convertValueDouble(txtValuePiece.getText());
        double value2 = Validation.convertValueDouble(txtValueHandWork.getText());
        double valueTotal = value1 + value2;
        txtValueTotal.setText(Double.toString(valueTotal));
    }//GEN-LAST:event_txtValueHandWorkMouseExited

    public void clearComp() {
        for (int i = 0; i < getContentPane().getComponentCount(); i++) {
            Component c = getContentPane().getComponent(i);
            if (c instanceof JTextField) {
                JTextField field = (JTextField) c;
                field.setText("");
            }
        }
    }

    public void addFilds(String[] data) {
        txtCpf.setText(data[0]);
        txtBrand.setText(data[1]);
        txtDate.setText(data[2]);
        txtDescription.setText(data[3]);
        txtEquipment.setText(data[4]);
        txtIdOs.setText(data[5]);
        txtModel.setText(data[6]);
        txtObservation.setText(data[7]);
        txtPiece.setText(data[8]);
        txtSerialNumber.setText(data[9]);
        txtValueHandWork.setText(data[10]);
        txtValuePiece.setText(data[11]);
        txtValueTotal.setText(data[12]);
        switch (data[13]) {
            case "Aprovado":
                rbtApproved.setSelected(true);
                break;
            case "Não Aprovada":
                rbtNotApproved.setSelected(true);
                break;
            case "Aguardando":
                rbtAwaiting.setSelected(true);
                break;
            case "Aguardando Orçamento":
                rbtAwaitingBudget.setSelected(true);
                break;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNew;
    private javax.swing.ButtonGroup btnRbtGrounp;
    private javax.swing.JButton btnReturn;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JMenu nav;
    private javax.swing.JMenuItem navClientQuery;
    private javax.swing.JMenuItem navQuerySystemOrder;
    private javax.swing.JMenuItem navRegisterClient;
    private javax.swing.JMenuItem navReturnHome;
    private javax.swing.JRadioButton rbtApproved;
    private javax.swing.JRadioButton rbtAwaiting;
    private javax.swing.JRadioButton rbtAwaitingBudget;
    private javax.swing.JRadioButton rbtNotApproved;
    private javax.swing.JTextField txtBrand;
    private javax.swing.JTextField txtCpf;
    private javax.swing.JFormattedTextField txtDate;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtEquipment;
    private javax.swing.JTextField txtIdOs;
    private javax.swing.JTextField txtModel;
    private javax.swing.JTextField txtObservation;
    private javax.swing.JTextField txtPiece;
    private javax.swing.JTextField txtSerialNumber;
    private javax.swing.JTextField txtValueHandWork;
    private javax.swing.JTextField txtValuePiece;
    private javax.swing.JTextField txtValueTotal;
    // End of variables declaration//GEN-END:variables
}

package Views;

import Controller.ControllerConsultOs;
import Utils.NameScreens;
import Utils.Validation;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ConsultOsJFrame extends javax.swing.JFrame {

    ControllerConsultOs controllerConsultOs;

    public ConsultOsJFrame(ControllerConsultOs c) {
        controllerConsultOs = c;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRbGrounpSelectFild = new javax.swing.ButtonGroup();
        btnRbGroupSelectField = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableOs = new javax.swing.JTable();
        btnChange = new javax.swing.JButton();
        rbtOs = new javax.swing.JRadioButton();
        rbtnAparatus = new javax.swing.JRadioButton();
        rbtnModel = new javax.swing.JRadioButton();
        rbtBrand = new javax.swing.JRadioButton();
        rbtnAll = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        navQuerySystemOrder = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        navReturnHome = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        nav = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        navRegisterClient = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        navClientQuery = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        btnRbGroupSelectField.setText("Pesquisar :");
        getContentPane().add(btnRbGroupSelectField);
        btnRbGroupSelectField.setBounds(33, 140, 70, 30);
        getContentPane().add(txtSearch);
        txtSearch.setBounds(110, 150, 190, 20);

        btnSearch.setText("Pesquisar");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        getContentPane().add(btnSearch);
        btnSearch.setBounds(310, 140, 100, 39);

        btnRemove.setText("Excluir");
        btnRemove.setPreferredSize(new java.awt.Dimension(104, 29));
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        getContentPane().add(btnRemove);
        btnRemove.setBounds(550, 140, 104, 39);

        btnBack.setText("Voltar");
        btnBack.setPreferredSize(new java.awt.Dimension(104, 29));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        getContentPane().add(btnBack);
        btnBack.setBounds(670, 140, 104, 39);

        jTableOs.setAutoCreateRowSorter(true);
        jTableOs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "OS", "Status", "Data", "Nome", "Cpf", "Aparelho", "Marca", "Modelo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableOs);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 294, 817, 190);

        btnChange.setText("Alterar");
        btnChange.setPreferredSize(new java.awt.Dimension(104, 29));
        btnChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeActionPerformed(evt);
            }
        });
        getContentPane().add(btnChange);
        btnChange.setBounds(430, 140, 104, 39);

        rbtOs.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGrounpSelectFild.add(rbtOs);
        rbtOs.setText("O.S");
        rbtOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtOsActionPerformed(evt);
            }
        });
        getContentPane().add(rbtOs);
        rbtOs.setBounds(230, 60, 50, 23);

        rbtnAparatus.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGrounpSelectFild.add(rbtnAparatus);
        rbtnAparatus.setText("Aparelho");
        rbtnAparatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAparatusActionPerformed(evt);
            }
        });
        getContentPane().add(rbtnAparatus);
        rbtnAparatus.setBounds(300, 60, 80, 23);

        rbtnModel.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGrounpSelectFild.add(rbtnModel);
        rbtnModel.setText("Modelo");
        rbtnModel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnModelActionPerformed(evt);
            }
        });
        getContentPane().add(rbtnModel);
        rbtnModel.setBounds(479, 60, 70, 23);

        rbtBrand.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGrounpSelectFild.add(rbtBrand);
        rbtBrand.setText("Marca");
        rbtBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtBrandActionPerformed(evt);
            }
        });
        getContentPane().add(rbtBrand);
        rbtBrand.setBounds(395, 60, 70, 23);

        rbtnAll.setBackground(new java.awt.Color(255, 255, 255));
        btnRbGrounpSelectFild.add(rbtnAll);
        rbtnAll.setText("Todos");
        rbtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAllActionPerformed(evt);
            }
        });
        getContentPane().add(rbtnAll);
        rbtnAll.setBounds(565, 60, 70, 23);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/supervision/image/Pagina_inicial3.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 820, 470);

        jMenu3.setText("Gerar O.S");

        navQuerySystemOrder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK));
        navQuerySystemOrder.setText("Cadastrar O.S");
        navQuerySystemOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navQuerySystemOrderActionPerformed(evt);
            }
        });
        jMenu3.add(navQuerySystemOrder);
        jMenu3.add(jSeparator5);

        navReturnHome.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        navReturnHome.setText("Voltar");
        navReturnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navReturnHomeActionPerformed(evt);
            }
        });
        jMenu3.add(navReturnHome);
        jMenu3.add(jSeparator6);

        jMenuBar1.add(jMenu3);

        nav.setText("Cadastro Cliente");
        nav.add(jSeparator2);

        navRegisterClient.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        navRegisterClient.setText("Cadastro Cliente");
        navRegisterClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navRegisterClientActionPerformed(evt);
            }
        });
        nav.add(navRegisterClient);
        nav.add(jSeparator1);

        navClientQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        navClientQuery.setText("Consulta Cliente");
        navClientQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navClientQueryActionPerformed(evt);
            }
        });
        nav.add(navClientQuery);
        nav.add(jSeparator4);

        jMenuBar1.add(nav);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(833, 544));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        Object[] result = null;
        String search = txtSearch.getText();
        String queryField = Validation.checkedGroup(btnRbGrounpSelectFild);
        DefaultTableModel modelo = (DefaultTableModel) jTableOs.getModel();
        modelo.setNumRows(0);
        modelo.rowsRemoved(null);
        try {
            result = controllerConsultOs.queryOs(queryField, search);

            if (result.length > 0 || result == null) {
                for (int i = 0; i < result.length; i++) {
                    modelo.addRow((Object[]) result[i]);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Não consta Registro com esse " + queryField);
                modelo.addRow(new Object[]{"Não consta Registro"});
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        controllerConsultOs.returnMain();
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        // TODO add your handling code here:
        int os = Integer.parseInt( jTableOs.getValueAt(jTableOs.getSelectedRow(), 0).toString());
        String del;
        Object[] result = null;
        String search = txtSearch.getText();
        String queryField = Validation.checkedGroup(btnRbGrounpSelectFild);
        DefaultTableModel modelo = (DefaultTableModel) jTableOs.getModel();
        modelo.setNumRows(0);
        modelo.rowsRemoved(null);
        try {
            del = controllerConsultOs.remove(os);
            result = controllerConsultOs.queryOs(queryField, search);

            if (result.length > 0 || result == null) {
                for (int i = 0; i < result.length; i++) {
                    modelo.addRow((Object[]) result[i]);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void navQuerySystemOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navQuerySystemOrderActionPerformed
        // TODO add your handling code here:
        controllerConsultOs.callScreens(NameScreens.RegisterServiceOrder);
    }//GEN-LAST:event_navQuerySystemOrderActionPerformed

    private void navReturnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navReturnHomeActionPerformed
        // TODO add your handling code here:
        controllerConsultOs.returnMain();
    }//GEN-LAST:event_navReturnHomeActionPerformed

    private void navRegisterClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navRegisterClientActionPerformed
        // TODO add your handling code here:
        controllerConsultOs.callScreens(NameScreens.RegisterClients);
    }//GEN-LAST:event_navRegisterClientActionPerformed

    private void navClientQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navClientQueryActionPerformed
        // TODO add your handling code here:
        controllerConsultOs.callScreens(NameScreens.ClientQuery);
    }//GEN-LAST:event_navClientQueryActionPerformed

    private void rbtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAllActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(false);
    }//GEN-LAST:event_rbtnAllActionPerformed

    private void rbtnModelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnModelActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_rbtnModelActionPerformed

    private void rbtBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtBrandActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_rbtBrandActionPerformed

    private void rbtnAparatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAparatusActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_rbtnAparatusActionPerformed

    private void rbtOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtOsActionPerformed
        // TODO add your handling code here:
        txtSearch.setEditable(true);
    }//GEN-LAST:event_rbtOsActionPerformed

    private void btnChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeActionPerformed
        // TODO add your handling code here:
        int os = Integer.parseInt( jTableOs.getValueAt(jTableOs.getSelectedRow(), 0).toString());
        try {
            controllerConsultOs.callChangeOs(os);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_btnChangeActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnChange;
    private javax.swing.ButtonGroup btnRbGrounpSelectFild;
    private javax.swing.JLabel btnRbGroupSelectField;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTableOs;
    private javax.swing.JMenu nav;
    private javax.swing.JMenuItem navClientQuery;
    private javax.swing.JMenuItem navQuerySystemOrder;
    private javax.swing.JMenuItem navRegisterClient;
    private javax.swing.JMenuItem navReturnHome;
    private javax.swing.JRadioButton rbtBrand;
    private javax.swing.JRadioButton rbtOs;
    private javax.swing.JRadioButton rbtnAll;
    private javax.swing.JRadioButton rbtnAparatus;
    private javax.swing.JRadioButton rbtnModel;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}

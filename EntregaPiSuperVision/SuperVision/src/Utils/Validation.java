package Utils;

import java.util.InputMismatchException;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;

/**
 *
 * @author Wellington
 */
public class Validation {

    /**
     *
     * @param cpf
     * @param i
     * @param i0
     * @return
     */
    private static String Mid(String cpf, int i, int i0) {
        return null;
    }

    /**
     *
     * @param fild
     * @return
     */
    public static boolean validateEmpty(String[] fild) {
        for (int i = 0; i < fild.length - 1; i++) {
            if (fild[i].isEmpty()) {
                return false;
            }
            if (fild[i].equalsIgnoreCase(" ")) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param cpf
     * @return
     */
    // Metodo usado para verificar se o CPF digitado é valido
    public static boolean checkCpf(String cpf) {
        if (cpf.equals("00000000000")
                || cpf.equals("11111111111")
                || cpf.equals("22222222222")
                || cpf.equals("33333333333")
                || cpf.equals("44444444444")
                || cpf.equals("55555555555")
                || cpf.equals("66666666666")
                || cpf.equals("77777777777")
                || cpf.equals("88888888888")
                || cpf.equals("99999999999")
                || (cpf.length() != 11)) {

            return (false);
        }

        char dig10, dig11;
        int sm, i, r, num, peso;


        // "try" - protege o codigo para eventuais erros de conversao de tipo (int) 
        try {
            // Calculo do 1o. Digito Verificador 
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero: 
                // por exemplo, transforma o caractere '0' no inteiro 0 
                // (48 eh a posicao de '0' na tabela ASCII) 
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            } else {
                dig10 = (char) (r + 48);
            }
            // converte no respectivo caractere numerico 
            // Calculo do 2o. Digito Verificador 
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            } else {
                dig11 = (char) (r + 48);
            }
            // Verifica se os digitos calculados conferem com os digitos informados. 
            if ((dig10 == cpf.charAt(9)) && (dig11 == cpf.charAt(10))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }

    }

    /**
     *
     * @param btnRbGroup
     * @return
     */
    public static String checkedGroup(ButtonGroup btnRbGroup) {
        for (Enumeration<AbstractButton> buttons = btnRbGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                return button.getText();
            }
        }
        return null;
    }

    /**
     *
     * @param number
     * @return
     */
    public static double convertValueDouble(String number) {
        if (number.isEmpty() == true) {
            return 0;
        }

        return Double.parseDouble(number);
    }

    /**
     *
     * @param number
     * @return
     */
    public static int convertValueInt(String number) {
        if (number.isEmpty()) {
            return 0;
        }

        return Integer.parseInt(number);
    }
}

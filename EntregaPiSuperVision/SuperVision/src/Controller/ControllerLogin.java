/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Models.ManageUser;
import Utils.ManagerXML;
import Views.HomeScreenJFrame;
import Views.LoginScreenJFrame;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.swing.JFrame;
/**
 *
 * @author aramos
 */
public class ControllerLogin {

    private LoginScreenJFrame login;
    private HomeScreenJFrame home;
    /**
     * 
     */
    
    public ControllerLogin() {
        ManagerXML.initialize();
        
        this.login = new LoginScreenJFrame(this);
        this.login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.login.setVisible(true);
        
    }
/**
 * 
 * @param login
 * @param password
 * @return
 * @throws GeneralSecurityException
 * @throws UnsupportedEncodingException
 * @throws IOException 
 */
    public String actionLogin(String login, String password ) throws GeneralSecurityException, UnsupportedEncodingException, IOException{
        
        ManageUser user = new ManageUser();
        if (user.validateLogin(login, password) == false){
            return "POR FAVOR, INFORME UM LOGIN/SENHA CORRETA!";
        }
        this.login.dispose();
        ControllerMain c = new ControllerMain();
        return "BEM VINDO AO SISTEMA!";
    }   
}

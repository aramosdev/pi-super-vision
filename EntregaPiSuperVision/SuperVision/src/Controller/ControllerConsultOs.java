/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.OrderOfService;
import Views.ConsultOsJFrame;
import javax.swing.JFrame;
import Models.ManagerOs;
import Utils.NameScreens;
import static Utils.NameScreens.ClientQuery;
import static Utils.NameScreens.RegisterClients;
import static Utils.NameScreens.RegisterServiceOrder;
import static Utils.NameScreens.ServiceOrderQuery;
import java.util.ArrayList;

/**
 *
 * @author aramos
 */
public class ControllerConsultOs {

    private ConsultOsJFrame consultOs;
    private ManagerOs managerOs;

    public ControllerConsultOs() {
        this.consultOs = new ConsultOsJFrame(this);
        this.consultOs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.consultOs.setVisible(true);
        managerOs = new ManagerOs();
    }
/**
 * 
 */
    public void returnMain() {
        ControllerMain controllerMain = new ControllerMain();
        this.consultOs.dispose();
    }
    /**
     * 
     * @param idOs
     * @return 
     */
    public String remove(int idOs){
        return managerOs.removeOs(managerOs.consultOS(idOs).get(0));
    }
    /**
     * 
     * @param idOs 
     */
    public void callChangeOs(int idOs){
        ControllerOs co = new ControllerOs(managerOs.consultOS(idOs).get(0));
        this.consultOs.dispose();
    }
    /**
     * 
     * @param query
     * @param element
     * @return 
     */
    public Object[] queryOs(String query, String element) {
        Object[] o = null;
        ArrayList<OrderOfService> orderOfServices;
        switch (query) {
            case "O.S":
                 orderOfServices = managerOs.consultOS(Integer.parseInt(element));
                 if(orderOfServices != null){
                    o = createJtable(orderOfServices);
                 }
            break;        
            case "Aparelho":
                  orderOfServices = managerOs.consultApparatus(element);
                  if(orderOfServices != null){
                    o = createJtable(orderOfServices);
                  }
            break;
            case "Marca":
                  orderOfServices = managerOs.queryBrand(element);
                  if(orderOfServices != null){
                    o = createJtable(orderOfServices);
                  }
            break;
            case "Modelo":
                  orderOfServices = managerOs.consultModel(element);
                  if(orderOfServices != null){
                    o = createJtable(orderOfServices);
                  }
            break;
            case "Todos":
                orderOfServices = managerOs.getOrderofservice();
                if(orderOfServices != null){
                    o = createJtable(orderOfServices);
                 }
            break;        
        }
        return o;

    }
/**
 * 
 * @param screens 
 */
    public void callScreens(NameScreens screens) {
        switch (screens) {
            case RegisterClients:
                ControllerClient controllerClient = new ControllerClient();
                this.consultOs.dispose();
                break;
            case ClientQuery:
                ControllerConsultClient controllerClientQuery = new ControllerConsultClient();
                this.consultOs.dispose();
                break;
            case RegisterServiceOrder:
                ControllerOs controllerOs = new ControllerOs();
                this.consultOs.dispose();
                break;
            case ServiceOrderQuery:
                ControllerConsultOs cos = new ControllerConsultOs();
                this.consultOs.dispose();
                break;
   
        }
    }
    /**
     * 
     * @param l
     * @return 
     */
    public Object[] createJtable(ArrayList<OrderOfService> l) {
        Object[] o = null;
        String data[] = new String[8];
        o = new Object[l.size()];
        for (int i = 0; i < o.length; i++) {
            o[i] = new Object[]{
                data[0] = Integer.toString(l.get(i).getOs()),
                data[1] = l.get(i).getStatus(),
                data[2] = l.get(i).getDate(),
                data[3] = l.get(i).getClient().getName(),
                data[4] = l.get(i).getClient().getCpf(),
                data[5] = l.get(i).getApparatus(),
                data[6] = l.get(i).getBrand(),
                data[7] = l.get(i).getModel()};
        }
        return o;
    }
}
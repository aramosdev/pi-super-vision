package Controller;

import Models.ManagerOs;
import Models.OrderOfService;
import Utils.NameScreens;
import static Utils.NameScreens.ClientQuery;
import static Utils.NameScreens.RegisterClients;
import static Utils.NameScreens.RegisterServiceOrder;
import static Utils.NameScreens.ServiceOrderQuery;
import Views.RegisterOrderService;
import javax.swing.JFrame;
/**
 * 
 * @author Wellington
 */
public class ControllerOs {

    private RegisterOrderService systemOrder;
    private ManagerOs managerOs;
    private OrderOfService orderOfService;

    public ControllerOs() {
        this.systemOrder = new RegisterOrderService(this);
        this.systemOrder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.systemOrder.setVisible(true);
        managerOs = new ManagerOs();
    }
    /**
     * 
     * @param os 
     */
    public ControllerOs(OrderOfService os) {
        String[] data = {
            os.getClient().getCpf(),
            os.getBrand(),
            os.getDate(),
            os.getDefect(),
            os.getApparatus(),
            Integer.toString(os.getOs()),
            os.getModel(),
            os.getObservation(),
            os.getPiece(),
            os.getSerialNumber(),
            Double.toString(os.getValueHandWork()),
            Double.toString(os.getValuePiece()),
            Double.toString(os.getValueTotal()),
            os.getStatus()
        };
        this.systemOrder = new RegisterOrderService(this);
        this.systemOrder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.systemOrder.addFilds(data);
        this.systemOrder.setVisible(true);
        managerOs = new ManagerOs();

    }

    /**
     * 
     * @param idOs
     * @param date
     * @param status
     * @param valuePiece
     * @param valueHandWork
     * @param valueTotal
     * @param observation
     * @param cpf
     * @param apparatus
     * @param brand
     * @param serialNumber
     * @param model
     * @param defect
     * @param piece
     * @return 
     */
    public String saveOs(int idOs, String date, String status, double valuePiece, double valueHandWork,
            double valueTotal, String observation, String cpf, String apparatus, String brand, 
            String serialNumber, String model, String defect, String piece) {

        orderOfService = new OrderOfService(idOs, date, status, valuePiece, valueHandWork, valueTotal, observation, apparatus, brand, serialNumber, model, defect);

        if (managerOs.consultOS(idOs).size()>0) {
            return managerOs.changeAllData(idOs, orderOfService, cpf);
        }

        return managerOs.saveOrderOfService(orderOfService, cpf);
    }
/**
 * 
 * @return 
 */
    public int getId() {
        managerOs = new ManagerOs();
        return managerOs.getId();
    }
/**
 * 
 */
    public void returnMain() {
        ControllerMain controllerMain = new ControllerMain();
        this.systemOrder.dispose();
    }
/**
 * 
 * @param idOs
 * @param date
 * @param status
 * @param valuePiece
 * @param valueHandWork
 * @param valueTotal
 * @param observation
 * @param cpf
 * @param apparatus
 * @param brand
 * @param serialNumber
 * @param model
 * @param defect
 * @param piece
 * @return 
 */
    public String newRegister(int idOs, String date, String status, double valuePiece, double valueHandWork, double valueTotal, String observation, String cpf,
            String apparatus, String brand, String serialNumber, String model, String defect, String piece) {

        orderOfService = new OrderOfService(idOs, date, status, valuePiece, valueHandWork, valueTotal, observation, apparatus, brand, serialNumber, model, defect);

        return managerOs.saveOrderOfService(orderOfService, cpf);

    }
/**
 * 
 * @param screens 
 */
    public void callScreens(NameScreens screens) {
        switch (screens) {
            case RegisterClients:
                ControllerClient controllerClient = new ControllerClient();
                this.systemOrder.dispose();
                break;
            case ClientQuery:
                ControllerConsultClient controllerClientQuery = new ControllerConsultClient();
                this.systemOrder.dispose();
                break;
            case RegisterServiceOrder:
                ControllerOs controllerOs = new ControllerOs();
                this.systemOrder.dispose();
                break;
            case ServiceOrderQuery:
                ControllerConsultOs consultOs = new ControllerConsultOs();
                this.systemOrder.dispose();
                break;
 
        }
    }
}

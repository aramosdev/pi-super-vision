/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Client;
import Views.ConsultClientJFrame;
import Models.ManagerClient;
import Models.ManagerOs;
import Utils.NameScreens;
import static Utils.NameScreens.ClientQuery;
import static Utils.NameScreens.RegisterClients;
import static Utils.NameScreens.RegisterServiceOrder;
import static Utils.NameScreens.ServiceOrderQuery;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author aramos
 */
public class ControllerConsultClient {

    private ConsultClientJFrame consultClient;
    private ManagerClient managerClient;
/**
 * 
 */
    public ControllerConsultClient() {
        this.consultClient = new ConsultClientJFrame(this);
        this.consultClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.consultClient.setVisible(true);
        managerClient = new ManagerClient();
    }
    /**
     * 
     */
    
    public void returnMain() {
        ControllerMain controllerMain = new ControllerMain();
        this.consultClient.dispose();
    }
    /**
     * 
     * @param cpf 
     */
    public void callChangeClient(String cpf){
        ControllerClient client = new ControllerClient(managerClient.cpfQuery(cpf).get(0));
        this.consultClient.dispose();
    }
    /**
     * 
     * @param cpf
     * @return 
     */
    public String remove(String cpf){
        Client c = managerClient.cpfQuery(cpf).get(0);
        return managerClient.remove(c);
    }
/**
 * 
 * @param query
 * @param element
 * @return 
 */
    public Object[] queryClient(String query, String element) {
        Object[] o = null;
        ArrayList<Client> clients;
        
        switch (query) {
            case "Nome":
                clients =managerClient.consultName(element);
                if(clients != null){
                    o = createJtable(clients);
                }
                break;
            case "CPF":
                clients =managerClient.cpfQuery(element);
                if(clients != null){
                    o = createJtable(clients);
                }
                break;
            case "RG":
                clients = managerClient.consultRG(element);
                if(clients != null){
                    o = createJtable(clients);
                }
                break;
            case "Todos":
                clients = managerClient.getClients();
                if(clients != null){
                    o = createJtable(clients);
                }
                break;
        }
        return o;
    }
    /**
     * 
     * @param screens 
     */

    public void callScreens(NameScreens screens) {
        switch (screens) {
            case RegisterClients:
                ControllerClient controllerClient = new ControllerClient();
                this.consultClient.dispose();
                break;
            case ClientQuery:
                ControllerConsultClient controllerClientQuery = new ControllerConsultClient();
                this.consultClient.dispose();
                break;
            case RegisterServiceOrder:
                ControllerOs controllerOs = new ControllerOs();
                this.consultClient.dispose();
                break;
            case ServiceOrderQuery:
                ControllerConsultOs consultOs = new ControllerConsultOs();
                this.consultClient.dispose();
                break;
  

        }
    }
/**
 * 
 * @param l
 * @return 
 */
    public Object[] createJtable(ArrayList<Client> l) {
        Object[] o = null;
        String data[] = new String[6];
        ManagerOs m = new ManagerOs();
        o = new Object[l.size()];
        for (int i = 0; i < o.length; i++) {
            o[i] = new Object[]{
                data[0] = l.get(i).getName(),
                data[1] = l.get(i).getCpf(),
                data[2] = l.get(i).getRg(),
                data[3] = l.get(i).getHomePhone() + " / " + l.get(i).getCellphone(),
                data[4] = Integer.toString(m.queryOsClient(l.get(i).getCpf())),
                data[5] = l.get(i).getStreet() + " - " + l.get(i).getNumber()
            };
        }
        return o;
    }
}

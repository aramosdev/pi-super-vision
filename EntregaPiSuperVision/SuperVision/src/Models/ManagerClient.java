/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import Utils.ManagerXML;
import Utils.Validation;

/**
 *
 * @author Wellington
 */
public class ManagerClient {

    private ArrayList<Client> clients;
    private ArrayList<Equipment> Equipments;
    private Client client;
    private int id;

/**
 * 
 */
    public ManagerClient() {

        clients = ManagerXML.readClients();

        if (clients != null && clients.isEmpty()) {
            id = 1;
        } else {
            id = clients.get(clients.size() - 1).getIdClient() + 1;
        }
    }
    /**
     * 
     * @return 
     */
    public ArrayList<Client> getClients() {
        return clients;
    }
    /**
     * 
     * @param client
     * @return 
     */

    public String saveClient(Client client) {

        String[] data = {client.getName(), client.getRg(), client.getStreet(), client.getNumber()};

        if (Validation.validateEmpty(data) == false) {
            return "POR FAVOR INFORME OS CAMPOS OBRIGATORIOS!";
        }

        if (Validation.checkCpf(client.getCpf()) == false) {
            return "INFORME UM CPF VALIDO!";
        }
        for (int i = 0; i < clients.size(); i++) {
            Client client1 = clients.get(i);
            if (client1.getRg().equalsIgnoreCase(client.getRg())) {
                return "RG JÁ CADASTRADO!";
            }
        }

        for (int i = 0; i < clients.size(); i++) {
            Client client1 = clients.get(i);
            if (client1.getCpf().equalsIgnoreCase(client.getCpf())) {
                return "CPF JÁ CADASTRADO!";
            }
        }
        client.setIdClient(id);
        id++;
        clients.add(client);
        ManagerXML.saveClients(clients);
        return "CLIENTE CADASTRADO COM SUCESSO";
    }
    /**
     * 
     * @param equip 
     */
    public void saveApparatus(Equipment equip){
        Equipments.add(equip);
        client.setEquipment(Equipments);
        ManagerXML.saveClients(clients);
    }
    /**
     * 
     * @param name
     * @return 
     */

    public ArrayList<Client> consultName(String name) {
        ArrayList<Client> c = new ArrayList<>();
        for (Client client1 : clients) {
            if (client1.getName().startsWith(name)) {
                c.add(client1);
            }
        }
        return c;
    }
/**
 * 
 * @param cpf
 * @return 
 */
    //metodo usado para consultar via CPF o cliente
    public ArrayList<Client> cpfQuery(String cpf) {
        ArrayList<Client> c = new ArrayList<>();
        if (!Validation.checkCpf(cpf)) {
            return null;
        }
        for (Client client1 : clients) {
            if (client1.getCpf().equalsIgnoreCase(cpf)) {
                c.add(client1);
            }
        }
        return c;
    }
/**
 * 
 * @param rg
 * @return 
 */
    public ArrayList<Client> consultRG(String rg) {
        ArrayList<Client> c = new ArrayList<>();
        for (Client client1 : clients) {
            if (client1.getRg().equalsIgnoreCase(rg)) {
                c.add(client1);
            }
        }
        return c;
    }

    /**
     * 
     * @param c
     * @return 
     */
    
    public String remove(Client c) {
        ManagerOs managerOs = new ManagerOs();
        int size = managerOs.getOrderofservice().size();
        for (int i = 0; i < size; i++) {
            OrderOfService o = managerOs.getOrderofservice().get(i);
            if (o.getClient().getCpf().equalsIgnoreCase(c.getCpf())) {
                return "CLIENTE NÃo SER REMOVIDO DO SISTEMA";
            }
        }
        clients.remove(c);
        ManagerXML.saveClients(clients);
        return "CLIENTE REMOVIDO COM SUCESSO";
    }
/**
 * 
 * @param cpf
 * @param client
 * @return 
 */
    public String changeAllData(String cpf, Client client) {

        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getCpf().equalsIgnoreCase(cpf)) {
                clients.set(i, client);
                ManagerXML.saveClients(clients);
                return "REGISTRO ALTERADO COM SUCESSO";
            }
        }
        return "Registro nao encontrado.s";

    }
}
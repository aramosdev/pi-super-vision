/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author Wellington
 */
public class Client extends Address {

    private int idClient;
    private String name;
    private String rg;
    private String cpf;
    private String homePhone;
    private String cellphone;    
    private ArrayList<Equipment> equipment;
    /**
     * 
     */

    public Client() {
    }
    /**
     * 
     * @param name
     * @param rg
     * @param cpf
     * @param homePhone
     * @param cellphone
     * @param street
     * @param number
     * @param neighborhood
     * @param zipCode
     * @param city
     * @param state 
     */
    public Client(String name, String rg, String cpf, String homePhone, String cellphone, String street, String number, String neighborhood, String zipCode, String city, String state) {
        super(street, number, neighborhood, zipCode, city, state);
        this.name = name;
        this.rg = rg;
        this.cpf = cpf;
        this.homePhone = homePhone;
        this.cellphone = cellphone;

    }
    
    
    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public ArrayList<Equipment> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<Equipment> equipment) {
        this.equipment = equipment;
    }
}

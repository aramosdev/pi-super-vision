/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.ManagerXML;
import Utils.ProtectedConfigFile;
import Utils.Validation;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

/**
 *
 * @author aramos
 */
public class ManageUser extends ProtectedConfigFile implements InterfaceLogin{
    private ManagerXML xml = new ManagerXML();
    private ArrayList<SystemUser> user = new ArrayList<>();
    /**
     * 
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException 
     */
    public ManageUser() throws GeneralSecurityException, UnsupportedEncodingException{
        saveUser("admin", "admin", "admin");
    }
    

    /**
     * 
     * @param name
     * @param login
     * @param password
     * @return
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException 
     */
    public boolean saveUser(String name, String login, String password) throws GeneralSecurityException, UnsupportedEncodingException{
        boolean checked = true;
        boolean checkEmpty = false;
        String[] data = {name, login, password}; 
        
        checkEmpty = Validation.validateEmpty(data);
        if (user.size() >0x0) {
            for (int i = 0; i < user.size(); i++) {
                SystemUser u = user.get(i);
                if(u.getLogin().equalsIgnoreCase(login)) checked = false;
            }
        }
        if ((checked == true) && (checkEmpty == true) ){
            addUser(name, login, password);
            return true;
        }
        
        return true;
    }
    /**
     * 
     * @param name
     * @param login
     * @param password
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException 
     */
    public void addUser(String name, String login, String password) throws GeneralSecurityException, UnsupportedEncodingException{
        user.add(new SystemUser(name, login, encrypt(password)));
        ManagerXML.saveUser(user);
    }
    
    /**
     * 
     * @param login
     * @param password
     * @return
     * @throws GeneralSecurityException
     * @throws IOException 
     */
    //method para validar login a se pensar em como fazer
    @Override
    public boolean validateLogin(String login, String password) throws GeneralSecurityException, IOException{
        for (int i = 0; i < user.size(); i++) {
            SystemUser u = user.get(i);
            if ((u.getLogin().equalsIgnoreCase(login))&&(decrypt(u.getPassword()).equalsIgnoreCase(password) )) {
                return true;
            }
        }
         return false;
    }
    
    /**
     * 
     * @param name
     * @param login
     * @return 
     */
    //method para consulta os dados do usuario do sistema 
    //esse method a consulta pelo nome e o login
    //(Falta criar Tela para alterar registro do usuario) 
    public SystemUser userQuery (String name, String login){
        if (!name.isEmpty()) {
            for (int i = 0; i <= user.size(); i++) {
                SystemUser u = user.get(i);
                if (name.equalsIgnoreCase(u.getName())) {
                    return u;
                }
            }
        }else if (!login.isEmpty()) {
            for (int i = 0; i < user.size(); i++) {
                SystemUser u = user.get(i);
                if (login.equalsIgnoreCase(u.getLogin())) {
                    return u;
                }
            }
        }
        return null;
    }
    
    /**
     * 
     * @param position
     * @return 
     */
    //method para consulta os dados do usuario do sistema 
    //esse method a consulta pelo nome e o login
    //(Falta criar Tela para alterar registro do usuario) 
    public String deleteUser(int position ){
        user.remove(position);
        xml.saveUser(user);
        return "OPERAÇÃo REALIZADA COM SUCESSO!";
    }
    
    
    /**
     * 
     * @param position
     * @param name
     * @param login
     * @param password
     * @return
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException 
     */
    public boolean savaChanges(int position, String name, String login, String password) throws GeneralSecurityException, UnsupportedEncodingException{
        boolean checked = true;
        boolean checkEmpty = false;
        SystemUser u = user.get(position);
        String[] data = {name, login, password}; 
        Validation valid = new Validation();
        checkEmpty = valid.validateEmpty(data);
        if (!u.getLogin().equalsIgnoreCase(login)) {
            for (int i = 0; i < user.size(); i++) {
                SystemUser u2 = user.get(i);
                if(u2.getLogin().equalsIgnoreCase(login)) checked = false;
            }
        }
        if ((checked == true) && (checkEmpty == true) ){
           addUser(name, login, password);
           return true;
        }
        return false;
        
    }
}

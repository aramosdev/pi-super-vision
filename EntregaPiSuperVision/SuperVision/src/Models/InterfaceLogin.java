/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

/**
 *
 * @author aramos
 */
public interface InterfaceLogin {
/**
 * 
 * @param name
 * @param login
 * @param password
 * @return
 * @throws GeneralSecurityException
 * @throws UnsupportedEncodingException 
 */
    public boolean saveUser(String name, String login, String password)
            throws GeneralSecurityException, UnsupportedEncodingException;
/**
 * 
 * @param login
 * @param password
 * @return
 * @throws GeneralSecurityException
 * @throws IOException 
 */
    public boolean validateLogin(String login, String password) throws GeneralSecurityException, IOException;
/**
 * 
 * @param name
 * @param login
 * @return 
 */
    public SystemUser userQuery(String name, String login);
/**
 * 
 * @param position
 * @return 
 */
    public String deleteUser(int position);
/**
 * 
 * @param position
 * @param name
 * @param login
 * @param password
 * @return
 * @throws GeneralSecurityException
 * @throws UnsupportedEncodingException 
 */
    public boolean savaChanges(int position, String name, String login, String password)
            throws GeneralSecurityException, UnsupportedEncodingException;
}
